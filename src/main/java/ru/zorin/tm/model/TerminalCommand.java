package ru.zorin.tm.model;

import ru.zorin.tm.constant.ArgumentConst;
import ru.zorin.tm.constant.TerminalConst;

public class TerminalCommand {

    public static final TerminalCommand HELP = new TerminalCommand(
            TerminalConst.CMD_HELP, ArgumentConst.ARG_HELP, "Show developer info"
    );

    public static final TerminalCommand ABOUT = new TerminalCommand(
            TerminalConst.CMD_ABOUT, ArgumentConst.ARG_ABOUT, "Show version info"
    );

    public static final TerminalCommand VERSION = new TerminalCommand(
            TerminalConst.CMD_VERSION, ArgumentConst.ARG_VERSION, "Show display message"
    );

    public static final TerminalCommand INFO = new TerminalCommand(
            TerminalConst.CMD_INFO, ArgumentConst.ARG_INFO, "Show display message"
    );

    public static final TerminalCommand EXIT = new TerminalCommand(
            TerminalConst.CMD_EXIT, null, "Close application"
    );

    private String name = "";
    private String arg = "";
    private String description = "";

    public TerminalCommand() {

    }

    public TerminalCommand(String name) {
        this.name = name;
    }

    public TerminalCommand(String name, String arg) {
        this.name = name;
        this.arg = arg;
    }

    public TerminalCommand(String name, String arg, String description) {
        this.name = name;
        this.arg = arg;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArg() {
        return arg;
    }

    public void setArg(String arg) {
        this.arg = arg;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        final StringBuilder result = new StringBuilder();
        if (name != null && !name.isEmpty()) result.append(name);
        if (arg != null && !arg.isEmpty()) result.append(", ").append(arg);
        if (description != null && !description.isEmpty()) result.append(": ").append(description);
        return result.toString();
    }

}