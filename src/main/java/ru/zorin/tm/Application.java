package ru.zorin.tm;

import ru.zorin.tm.constant.ArgumentConst;
import ru.zorin.tm.constant.TerminalConst;
import ru.zorin.tm.model.TerminalCommand;
import ru.zorin.tm.repository.CommandRepository;
import ru.zorin.tm.util.NumberUtil;

import java.util.Arrays;
import java.util.Scanner;

public class Application {

    public static CommandRepository COMMAND_REPOSITORY = new CommandRepository();

    public static void main(String[] args) {
        System.out.println("***WELCOME TO TASK MANAGER***");
        if (parseArgs(args)) System.exit(0);
        while (true) {
            final Scanner scanner = new Scanner(System.in);
            final String command = scanner.nextLine();
            parseCommand(command);
        }
    }

    private static void parseArg(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConst.ARG_HELP:
                showHelp();
                break;
            case ArgumentConst.ARG_ABOUT:
                showAbout();
                break;
            case ArgumentConst.ARG_VERSION:
                showVersion();
                break;
            case ArgumentConst.ARG_INFO:
                showInfo();
                break;
        }
    }

    private static void parseCommand(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case TerminalConst.CMD_HELP:
                showHelp();
                break;
            case TerminalConst.CMD_ABOUT:
                showAbout();
                break;
            case TerminalConst.CMD_VERSION:
                showVersion();
                break;
            case TerminalConst.CMD_INFO:
                showInfo();
                break;
            case TerminalConst.CMD_ARGUMENTS:
                showArguments();
                break;
            case TerminalConst.CMD_COMMANDS:
                showCommands();
                break;
            case TerminalConst.CMD_EXIT:
                exit();
                break;
        }
    }

    private static boolean parseArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.println(TerminalCommand.HELP);
        System.out.println(TerminalCommand.VERSION);
        System.out.println(TerminalCommand.ABOUT);
        System.out.println(TerminalCommand.EXIT);
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Developer name: Vsevolod Zorin");
        System.out.println("E-mail: seva89423@gmail.com");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.4");
    }

    public static void showInfo() {
        System.out.println("[INFO]");

        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors: " + availableProcessors);

        final long freeMemory = Runtime.getRuntime().freeMemory();
        final String freeMemoryFormat = NumberUtil.formatBytes(freeMemory);
        System.out.println("Free memory: " + freeMemoryFormat);

        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryFormat = (maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue);
        System.out.println("Maximum memory: " + maxMemoryFormat);

        final long totalMemory = Runtime.getRuntime().totalMemory();
        final String totalMemoryFormat = NumberUtil.formatBytes(totalMemory);
        System.out.println("Total memory available to JVM: " + totalMemoryFormat);
        final long usedMemory = totalMemory - freeMemory;
        final String usedMemoryFormat = NumberUtil.formatBytes(usedMemory);
        System.out.println("Used memory by JVM: " + usedMemoryFormat);

    }

    public static void showCommands() {
        final String[] commands = COMMAND_REPOSITORY.getCommands();
        System.out.println(Arrays.toString(commands));
    }

    public static void showArguments() {
        final String[] arguments = COMMAND_REPOSITORY.getArgs();
        System.out.println(Arrays.toString(arguments));
    }

    public static void exit() {
        System.exit(0);
    }

}